# API Evaluation DevOps Estia 2020

API repository for the evaluation project

## Get started

Clone this repo

```sh 
git clone git@https://github.com/test_API_eval.git
```


### Use Docker Compose

Build Container

```sh
docker-compose build
```

Launch containers (1st term)

```sh 
docker-compose up
```

Enter container, install dependencies and start API (2nd term)

```sh 
docker-compose exec api bash
yarn
yarn dev
```

Run test in API container (3rd term)

```sh 
docker-compose exec api bash
yarn test
```

### Troubleshootings

If you have build Permission errors, use `sudo`