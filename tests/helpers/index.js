'use strict';

require('../cfg');
const { Todo } = require('../cfg');

function createTodo(todo) {
  if (!todo) {
    todo = {title: 'Test Todo List'};
  }

  return Todo.create(todo);
}

module.exports = {createTodo};
