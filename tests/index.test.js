'use strict';

const test = require('unit.js');

describe('Home', function() {
  it('should print Kaixo mundua !', function(done) {
    test
      .httpAgent('http://localhost:8080')
      .get('/')
      .expect(200)
      .end(function(err, res) {
        if (err) {
          done(err);
        }
        
        test
          .string(res.text)
            .is('Kaixo mundua !')
        ;

        done();
      })
    ;
  });

  it('should not print Hello World!', function(done) {
    test
      .httpAgent('http://localhost:8080')
      .get('/')
      .expect(200)
      .end(function(err, res) {
        if (err) {
          done(err);
        }
        
        test
          .string(res.text)
            .isNot('Hello World!')
        ;

        done();
      })
    ;
  });
});