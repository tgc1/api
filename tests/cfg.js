'use strict';

const mongoose = require('mongoose');
const Todo = require('../src/models/todo');

mongoose.connection.on('error', function(err) {
  console.error('DB connection:', err);
});

mongoose.connection.once('connected', function() {
  console.info('Mongoose connected to MongoDB server');
});

mongoose.connection.once('closed', function() {
  console.log('Mongoose connection closed');
});

function connectMongoose() {
  return mongoose.connect(`mongodb://${process.env.MONGO_SERVER}/api-eval`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    keepAlive: true,
    keepAliveInitialDelay: 300000,
  });
}

function connectMongoDB(done) {
  return connectMongoose()
    .then(() => {
      done();
    })
    .catch((err) => {
      console.error(err);
      done(err);
    });
}

function closeMongoDB(done) {
  mongoose.connection.close(function() {
    console.log('Mongoose connection closed gracefully');
    done();
  });
}

global.apiUrl = 'http://localhost:8080';
global.connectMongoDB = connectMongoDB;
global.closeMongoDB = closeMongoDB;
global.mongoose = mongoose;

module.exports = {Todo};
