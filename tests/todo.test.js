/* eslint-disable no-unused-expressions */
'use strict';

const test = require('unit.js');
const {Todo} = require('./cfg');
const { createTodo } = require('./helpers');

describe('Todos', function() {
  before(function(done) {
    connectMongoDB(done);
  });

  after(function(done) {
    closeMongoDB(done);
  });

  beforeEach(function(done) {
    Todo
      .deleteMany({})
      .then(() => done())
      .catch(done);
  });

  it('should create a todo', async function() {
    const res = await test
      .httpAgent(apiUrl)
      .post('/todos')
      .send({title: 'todo de test'})
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
    ;

    test
      .object(res.body)
        .isNotEmpty()
      .string(res.body.id)
        .hasLength(24)
      .string(res.body.title)
        .isEqualTo('todo de test')
      .string(res.body.createdAt)
      .bool(res.body.createdAt <= Date.now)
      .string(res.body.updatedAt)
        .isEqualTo(res.body.createdAt)
    ;
  });

  it('should update a todo', async function() {
    let createdTodo = await createTodo();
    let newTodo = {title: 'mon todo modifié'};

    const res = await test
      .httpAgent(apiUrl)
      .put('/todos/' + createdTodo.id)
      .send(newTodo)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    ;

    test
      .object(res.body)
        .isNotEmpty()
      .string(res.body.id)
        .hasLength(24)
      .string(res.body.title)
        .isEqualTo(newTodo.title)
      .bool(res.body.createdAt < res.body.createdAt)
    ;
  });

  it('should delete a todo', async function() {
    let createdTodo = await createTodo();

    await test
      .httpAgent(apiUrl)
      .delete('/todos/' + createdTodo.id)
      .set('Accept', 'application/json')
      .expect(200)
    ;

    await test
      .httpAgent(apiUrl)
      .get('/todos/' + createdTodo.id)
      .expect('Content-Type', /json/)
      .expect(404)
    ;
  });

  it('should list all todos', async function() {
    let createdTodo = await createTodo();
    let res = await test
      .httpAgent(apiUrl)
      .get('/todos')
      .expect('Content-Type', /json/)
      .expect(200)
    ;

    // With Unit.js
    test
      .array(res.body)
      .hasLength(1)

      .object(res.body[0])
        .hasProperty('id', createdTodo.id)
        .hasProperty('title', createdTodo.title)
      ;
  });

  it('should show one todo', async function() {
    let createdTodo, todo, res;

    createdTodo = await createTodo();

    res = await test
      .httpAgent(apiUrl)
      .get('/todos/' + createdTodo.id)
      .expect('Content-Type', /json/)
      .expect(200)
    ;

    test
      .object(res.body)
        .isNotEmpty()
      .string(res.body.id)
        .hasLength(24)
        .isEqualTo(createdTodo.id)
      .string(res.body.title)
        .isEqualTo(createdTodo.title)
    ;
  });
});
