/**
 * Todo model
 */

'use strict';

// eslint-disable-next-line no-redeclare
let mongoose = require('mongoose');

const Schema = mongoose.Schema;


/*----------------------------------------------------------------------------*\
  Schema
\*----------------------------------------------------------------------------*/

/**
 * Todo Schema
 * @constructor Todo
 */
const TodoSchema = new Schema(/** @lends Todo.prototype */ {
  // meta data
  createdAt: {
    type: Date,
    default: Date.now,
  },

  updatedAt: {
    type: Date,
    default: Date.now,
  },

  // content
  title: {
    type: String,
    required: true,
    minLength: 2,
    maxLength: 50,
  },
}, 
{
  timestamps: true,
});


/*----------------------------------------------------------------------------*\
  Increase
\*----------------------------------------------------------------------------*/

TodoSchema.virtual('id').get(function() {
  return this._id.toString();
});

/*----------------------------------------------------------------------------*\
  Expose
\*----------------------------------------------------------------------------*/

// JSON serialization
TodoSchema.set('toJSON', {
  getters: true,
  virtuals: true,
});

module.exports = mongoose.model('Todo', TodoSchema);
