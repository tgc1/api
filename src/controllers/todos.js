'use strict';

const httpError = require('http-errors');
const Todo = require('../models/todo');
const ctrl = {};

/**
 * Display all todos.
 *
 * @param  {Request}  req
 * @param  {Response} res
 * @param  {function} next
 */
ctrl.index = function index(req, res, next) {
  Todo
    .find()
    .sort({createdAt: 'desc'})
    .then(function(todos) {
      return res.status(200).send(todos);
    })
    .catch(next)
  ;
};

/**
 * Create a new todo.
 *
 * @param  {Request}  req
 * @param  {Response} res
 * @param  {function} next
 */
ctrl.create = function create(req, res, next) {
  Todo
    .create(req.body)
    .then(function(todo) {
      return res.status(201).send(todo);
    })
    .catch(next)
  ;
};

/**
 * Update a todo.
 *
 * @param  {Request}  req
 * @param  {Response} res
 * @param  {function} next
 */
ctrl.update = function update(req, res, next) {
  Todo
    .findOneAndUpdate(
      {_id: req.params.id}, 
      req.body, 
      {new: true}
    )
    .then(function(todo) {
      if (todo) {
        return res.status(200).send(todo);
      }

      throw httpError(404, 'Cannot find todo: ' + req.params.id);
    })
    .catch(next)
  ;
};

/**
 * Remove a given todo.
 *
 * @param  {Request}  req
 * @param  {Response} res
 * @param  {function} next
 */
ctrl.destroy = function destroy(req, res, next) {
  Todo
    .findOneAndRemove({_id: req.params.id})
    .then(function(todo) {
      return res.status(200).send(todo);
    })
    .catch(next)
  ;
};

/**
 * Display a given todo.
 *
 * @param  {Request}  req
 * @param  {Response} res
 * @param  {function} next
 */
ctrl.show = function show(req, res, next) {
  Todo
    .findById(req.params.id)
    .then(function(todo) {
      if (todo) {
        return res.status(200).send(todo);
      }

      throw next(httpError(404, 'Cannot find todo: ' + req.params.id));
    })
    .catch(next)
  ;
};

module.exports = ctrl;
