'use strict';

let router   = api.Router();
let todoCtrl = require('../controllers/todos');

router.get('/', todoCtrl.index);
router.post('/', todoCtrl.create);
router.get('/:id', todoCtrl.show);
router.put('/:id', todoCtrl.update);
router.delete('/:id', todoCtrl.destroy);

api.app.use('/todos', router);