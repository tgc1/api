FROM node:12.18.2

WORKDIR /usr/api

COPY ./ ./

RUN yarn
EXPOSE 8080